output "id" {
   id = digitalocean_kubernetes_cluster.wil_wednesday.id
}


output "cluster_subnet" {

   cluster_subnet = digitalocean_kubernetes_cluster.wil_wednesday.cluster_subnet
}

output "raw_config" {
  raw_config = digitalocean_kubernetes_cluster.wil_wednesday.raw_config
} 




#id - A unique ID that can be used to identify and reference a Kubernetes cluster.
#cluster_subnet - The range of IP addresses in the overlay network of the Kubernetes cluster.
#service_subnet - The range of assignable IP addresses for services running in the Kubernetes cluster.
#ipv4_address - The public IPv4 address of the Kubernetes master node. This will not be set if high availability is configured on the cluster (v1.21+)
#endpoint - The base URL of the API server on the Kubernetes master node.
#status - A string indicating the current status of the cluster. Potential values include running, provisioning, and errored.
#created_at - The date and time when the Kubernetes cluster was created.
#updated_at - The date and time when the Kubernetes cluster was last updated.
#auto_upgrade - A boolean value indicating whether the cluster will be automatically upgraded to new patch releases during its maintenance window.
#kube_config.0 - A representation of the Kubernetes cluster's kubeconfig with the following attributes:
#raw_config - The full contents of the Kubernetes cluster's kubeconfig file.
#host - The URL of the API server on the Kubernetes master node.
#cluster_ca_certificate - The base64 encoded public certificate for the cluster's certificate authority.
#token - The DigitalOcean API access token used by clients to access the cluster.
#client_key - The base64 encoded private key used by clients to access the cluster. Only available if token authentication is not supported on your cluster.
#client_certificate - The base64 encoded public certificate used by clients to access the cluster. Only available if token authentication is not supported on your cluster.
#expires_at - The date and time when the credentials will expire and need to be regenerated.
#node_pool - In addition to the arguments provided, these additional attributes about the cluster's default node pool are exported:
#id - A unique ID that can be used to identify and reference the node pool.
#actual_node_count - A computed field representing the actual number of nodes in the node pool, which is especially useful when auto-scaling is enabled.
#nodes - A list of nodes in the pool. Each node exports the following attributes:
#id - A unique ID that can be used to identify and reference the node.
#name - The auto-generated name for the node.
#status - A string indicating the current status of the individual node.
#droplet_id - The id of the node's droplet
#created_at - The date and time when the node was created.
#updated_at - The date and time when the node was last updated.
#taint - A block representing a taint applied to all nodes in the pool. Each taint exports the following attributes (taints must be unique by key and effect pair):
#key - An arbitrary string. The "key" and "value" fields of the "taint" object form a key-value pair.
#value - An arbitrary string. The "key" and "value" fields of the "taint" object form a key-value pair.
#effect - How the node reacts to pods that it won't tolerate. Available effect values are: "NoSchedule", "PreferNoSchedule", "NoExecute".
#urn - The uniform resource name (URN) for the Kubernetes cluster.
#maintenance_policy - A block representing the cluster's maintenance window. Updates will be applied within this window. If not specified, a default maintenance window will be chosen.
#day - The day of the maintenance window policy. May be one of "monday" through "sunday", or "any" to indicate an arbitrary week day.
#duration A string denoting the duration of the service window, e.g., "04:00".
#start_time The hour in UTC when maintenance updates will be applied, in 24 hour format (e.g. “16:00”).

